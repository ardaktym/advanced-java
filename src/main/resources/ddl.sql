CREATE DATABASE postgres;
CREATE TABLE group_tb(
                         group_id INT PRIMARY KEY,
                         name VARCHAR(255)
);
CREATE TABLE student_tb(
                           student_id INT PRIMARY KEY,
                           name VARCHAR(255),
                           phone DECIMAL,
                           group_id INT,
                           FOREIGN KEY(group_id) REFERENCES group_tb(group_id)
);