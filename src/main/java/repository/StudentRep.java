package repository;

import java.sql.*;

public class StudentRep {
    String url = "jdbc:postgresql://localhost:5432/task2";
    String user = "postgres";
    String password = "123";
    Connection connection = null;

    public void printStudents(){
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, user, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT  * From student_tb");
            System.out.println("List of Students with their data");
            while(resultSet.next()){
                System.out.println(resultSet.getInt("student_id") + " | " + resultSet.getString("name")+ " | " + resultSet.getLong("phone")+ " | " + resultSet.getInt("group_id"));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(connection != null){
                try{
                    connection.close();
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
