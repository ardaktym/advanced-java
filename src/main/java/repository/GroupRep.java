package repository;

import java.sql.*;

public class GroupRep {
    String url = "jdbc:postgresql://localhost:5432/task2";
    String user = "postgres";
    String password = "123";
    Connection connection = null;

    public void printGroups(){
        try{
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, user, password);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT  * From group_tb");
            System.out.println("List of groups");
            System.out.println("id| name " );
            while(resultSet.next()){
                System.out.println(resultSet.getInt("group_id") + " | " + resultSet.getString("name"));
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            if(connection != null){
                try{
                    connection.close();
                }catch (SQLException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
