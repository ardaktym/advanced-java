package entity;

import lombok.Data;

@Data
public class Student {
    private int student_id;
    private String name;
    private Long phone;
    private int group_id;

}
