import entity.Student;
import repository.GroupRep;
import repository.StudentRep;

import java.sql.*;

public class Main {
    public static void main(String[] args)  {
        GroupRep groupRep = new GroupRep();
        StudentRep studentRep = new StudentRep();
        studentRep.printStudents();
        groupRep.printGroups();
    }

}
